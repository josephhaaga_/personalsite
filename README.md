This is my (Joseph Haaga) personal site source code. It's the StartBootstrap Freelancer theme with some CSS animations and my personal contact info/urls. 

- Contact form is a php script in the /mail directory
- MyWordpressPosts/wordpressPosts.js pulls latest blog headlines from haagajoe WordPress

TO DO:
- Set up A/B testing w/ Google Analytics
	- test Contact Me before/after Wordpress section
	- test number of items
	- test color schemes 
